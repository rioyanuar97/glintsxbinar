const fs = require("fs");

const readfile = (file, option) =>
  new Promise((fulfil, reject) => {
    fs.readFile(file, option, (err, content) => {
      if (err) throw err;
      return fulfil(content);
    });
  });

const readAllfiles = async () => {
  try {

    let data = await Promise.all([
        readfile("./contents/content1.txt", "utf-8"),
        readfile("./contents/content2.txt", "utf-8"),
        readfile("./contents/content3.txt", "utf-8"),
        readfile("./contents/content4.txt", "utf-8"),
        readfile("./contents/content5.txt", "utf-8"),
        readfile("./contents/content6.txt", "utf-8"),
        readfile("./contents/content7.txt", "utf-8"),
        readfile("./contents/content8.txt", "utf-8"),
        readfile("./contents/content9.txt", "utf-8"),
        readfile("./contents/content10.txt", "utf-8"),
    ])
    for (let i = 0; i < data.length; i++) {
      console.log(data[i]);
    }  
    // let content1 = await readfile("./contents/content1.txt", "utf-8");
    // console.log(content1);
    // let content2 = await readfile("./contents/content2.txt", "utf-8");
    // console.log(content2);
    // let content3 = await readfile("./contents/content3.txt", "utf-8");
    // console.log(content3);
    // let content4 = await readfile("./contents/content4.txt", "utf-8");
    // console.log(content4);
    // let content5 = await readfile("./contents/content5.txt", "utf-8");
    // console.log(content5);
    // let content6 = await readfile("./contents/content6.txt", "utf-8");
    // console.log(content6);
    // let content7 = await readfile("./contents/content7.txt", "utf-8");
    // console.log(content7);
    // let content8 = await readfile("./contents/content8.txt", "utf-8");
    // console.log(content8);
    // let content9 = await readfile("./contents/content9.txt", "utf-8");
    // console.log(content9);
    // let content10 = await readfile("./contents/content10.txt", "utf-8");
    // console.log(content10);
  } catch (e) {
    console.log("What error!");
  }
};

readAllfiles();
