const fs = require("fs");

const readfile = (file, option) =>
  new Promise((fulfil, reject) => {
    fs.readFile(file, option, (err, content) => {
      if (err) throw err;
      return fulfil(content);
    });
  });

readfile("./contents/content1.txt", "utf-8")
  .then((content) => {
    console.log(content);
    return readfile("./contents/content2.txt", "utf-8");
  })
  .then((content2) => {
    console.log(content2);
    return readfile("./contents/content3.txt", "utf-8");
  })
  .then((content3) => {
    console.log(content3);
    return readfile("./contents/content4.txt", "utf-8");
  })
  .then((content4) => {
    console.log(content4);
    return readfile("./contents/content5.txt", "utf-8");
  })
  .then((content5) => {
    console.log(content5);
    return readfile("./contents/content6.txt", "utf-8");
  })
  .then((content6) => {
    console.log(content6);
    return readfile("./contents/content7.txt", "utf-8");
  })
  .then((content7) => {
    console.log(content7);
    return readfile("./contents/content8.txt", "utf-8");
  })
  .then((content8) => {
    console.log(content8);
    return readfile("./contents/content9.txt", "utf-8");
  })
  .then((content9) => {
    console.log(content9);
    return readfile("./contents/content10.txt", "utf-8");
  })
  .then((content10) => {
    console.log(content10);
  })
  .catch((err) => console.error(err));
