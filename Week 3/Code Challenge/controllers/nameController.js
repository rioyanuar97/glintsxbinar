class NameController {
  get(req, res) {
    console.log("Get data");
    if (req.params.name == "Riko") res.send(`Wangi Riko Yanuar`);
    else {
      res.send("Nama Salah !");
    }
  }
  post(req, res) {
    console.log("Post data");
    if (req.params.name == "Riko") res.send(`Wangi Riko Yanuar`);
    else {
      res.send("Nama Salah !");
    }
  }
  put(req, res) {
    console.log("Put data");
    if (req.params.name == "Riko") res.send(`Wangi Riko Yanuar`);
    else {
      res.send("Nama Salah !");
    }
  }
  delete(req, res) {
    console.log("Delete data");
    if (req.params.name == "Riko") res.send(`Wangi Riko Yanuar`);
    else {
      res.send("Nama Salah !");
    }
  }
}

module.exports = new NameController();
