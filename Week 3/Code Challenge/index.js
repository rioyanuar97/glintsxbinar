const express = require("express");
const app = express();
const nameRouter = require("./routes/nameRoute");

app.use("/", nameRouter);

app.listen(3000, () => console.log("Server runnin on 3000"));
