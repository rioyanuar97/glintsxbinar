const express = require("express");
const router = express.Router();
const NameController = require("../controllers/nameController");


router.get('/:name', NameController.get);
router.post("/:name", NameController.post);
router.put("/:name", NameController.put);
router.delete("/:name", NameController.delete);


module.exports = router;
