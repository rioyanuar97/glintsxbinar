const Geometry = require("../geometry");

class ThreeDimension extends Geometry {
  constructor(name) {
    //make abstract class
    super(name, "3D");
    if (this.constructor == ThreeDimension) {
      throw new Error("Can not make object");
    }
  }
  introduce(intro) {
    super.introduce();
    console.log(`This is ${this.type}`);
  }
  calculateVolume() {
    console.log(`${this.name} Volume!`);
  }
}

module.exports = ThreeDimension;
