const ThreeDimension = require("./threeDimension");

class Tube extends ThreeDimension {
  constructor(radius, height) {
    super("Tube");
    this.radius = radius;
    this.height = height;
  }
  introduce(intro) {
    super.introduce();
    console.log(`This ${intro} is ${this.name}\n`);
  }
  calculateVolume() {
    super.calculateVolume();
    let volume = Math.PI * this.radius ** 2 * this.height;

    console.log(`This volume is ${volume.toFixed(2)} cm3\n`);
  }
}

module.exports = Tube;
