const ThreeDimension = require("./threeDimension");

class Beam extends ThreeDimension {
  constructor(length, width, height) {
    super("Beam");
    this.length = length;
    this.width = width;
    this.height = height;
  }
  introduce(intro) {
    super.introduce();
    console.log(`This ${intro} is ${this.name}\n`);
  }
  calculateVolume() {
    super.calculateVolume();
    let volume = this.length * this.width * this.height;

    console.log(`This volume is ${volume} cm3\n`);
  }
}

module.exports = Beam;
