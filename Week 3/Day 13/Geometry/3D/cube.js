const ThreeDimension = require("./threeDimension");

class Cube extends ThreeDimension {
  constructor(length) {
    super("Cube");
    this.length = length;
  }
  introduce(intro) {
    super.introduce();
    console.log(`This ${intro} is ${this.name}\n`);
  }
  calculateVolume() {
    super.calculateVolume();
    let volume = this.length**3;

    console.log(`This volume is ${volume} cm3\n`);
  }
}

module.exports = Cube;
