const ThreeDimension = require("./threeDimension");

class Cone extends ThreeDimension {
  constructor(radius, height) {
    super("Cone");
    this.radius = radius;
    this.height = height;
  }
  introduce(intro) {
    super.introduce();
    console.log(`This ${intro} is ${this.name}\n`);
  }
  calculateVolume() {
    super.calculateVolume();
    let volume = (Math.PI * this.radius ** 2 * this.height) / 3;

    console.log(`This volume is ${volume.toFixed(2)} cm3\n`);
  }
}

module.exports = Cone;
