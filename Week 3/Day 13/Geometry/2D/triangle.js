const TwoDimension = require("./twoDimension");

class Triangle extends TwoDimension {
  constructor(base, height) {
    super("Triangle");
    this.base = base;
    this.height = height;
  }
  introduce(intro) {
    super.introduce();
    console.log(`This ${intro} is ${this.name}\n`);
  }
  calculateArea() {
    super.calculateArea();
    let area = this.base * this.height / 2;
    console.log(`This area is ${area} cm2\n`);
  }
  calculateCircumference() {
    super.calculateCircumference();
    let circum = 3 * this.base;
    console.log(`This circumference is ${circum} cm\n`);
  }
}

// let square1 = new Triangle(12, 8);
// console.log(square1);
// square1.introduce("Geometry");
// square1.calculateArea();
// square1.calculateCircumference();
module.exports = Triangle;
