const Geometry = require("../geometry");

class TwoDimension extends Geometry {
  constructor(name) {
      //make abstract class
    super(name, "2D");
    if (this.constructor == TwoDimension) {
      throw new Error("Can not make object");
    }
  }
  introduce(intro) {
    super.introduce();
    console.log(`This is ${this.type}`);
  }
  calculateArea() {
    console.log(`${this.name} Area!`);
  }
  calculateCircumference() {
    console.log(`${this.name} Circumference!`);
  }
}

module.exports = TwoDimension;
