const TwoDimension = require("./twoDimension");

class Square extends TwoDimension {
  constructor(length) {
    super("Square");
    this.length = length;
  }
  introduce(intro) {
    super.introduce();
    console.log(`This ${intro} is ${this.name}\n`);
  }
  calculateArea() {
    super.calculateArea();
    let area = this.length ** 2;

    console.log(`This area is ${area} cm2\n`);
  }
  calculateCircumference() {
    super.calculateCircumference();
    let circum = 4 * this.length;

    console.log(`This circumference is ${circum} cm\n`);
  }
}

// let square1 = new Square(15);
// console.log(square1);
// square1.introduce("Geometry");
// square1.calculateArea();
// square1.calculateCircumference();
module.exports = Square;