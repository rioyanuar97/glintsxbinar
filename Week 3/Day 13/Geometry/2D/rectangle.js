const TwoDimension = require("./twoDimension");

class Rectangle extends TwoDimension {
  constructor(length,width) {
    super("Rectangle");
    this.length = length;
    this.width = width;
  }
  introduce(intro) {
    super.introduce();
    console.log(`This ${intro} is ${this.name}\n`);
  }
  calculateArea() {
    super.calculateArea();
    let area = this.length *this.width;

    console.log(`This area is ${area} cm2\n`);
  }
  calculateCircumference() {
    super.calculateCircumference();
    let circum = 2 * this.length + 2*this.width;

    console.log(`This circumference is ${circum} cm\n`);
  }
}

// let square1 = new Rectangle(10,5);
// console.log(square1);
// square1.introduce("Geometry");
// square1.calculateArea();
// square1.calculateCircumference();
module.exports = Rectangle;