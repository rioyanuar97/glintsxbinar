const Square = require("./2D/square");
const Rectangle = require("./2D/rectangle");
const Triangle = require("./2D/triangle");
const Beam = require("./3D/beam");
const Cube = require("./3D/cube");
const Tube = require("./3D/tube");
const Cone = require("./3D/cone");

module.exports = { Square, Rectangle, Triangle, Beam, Cube, Tube, Cone };
