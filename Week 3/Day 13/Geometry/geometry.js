class Geometry {
  constructor(name, type) {
      //make abstract class
    if (this.constructor == Geometry) {
      throw new Error("Can not make object");
    }
    this.name = name;
    this.type = type;
  }
  introduce() {
    this.#accessIntroduce();
  }
  //make private function(can only be access in here)
  #accessIntroduce() {
    console.log(`\nHello, this is Geometry !`);
  }
}

module.exports = Geometry;
