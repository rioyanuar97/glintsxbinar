//tidak perlu tulis index karena default sudah mencari index.js
const {
  Square,
  Rectangle,
  Triangle,
  Beam,
  Cube,
  Tube,
  Cone,
} = require("./Geometry/index");
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

console.log("Calculate Geometry")
console.log("------------------")
console.log("1. Square")
console.log("2. Rectangle")
console.log("3. Triangle")
console.log("4. Beam")
console.log("5. Cube")
console.log("6. Tube")
console.log("7. Cone")
console.log("------------------")
rl.question("Choose Geometry : ", (option) => {
  switch (option) {
    case '1':
      rl.question("Length : ", (length) => {
        let squareOne = new Square(length);
        squareOne.introduce("Geometry");
        squareOne.calculateArea();
        squareOne.calculateCircumference();
        rl.close();
      });
      break;
    case '2':
      rl.question("Length : ", (length) => {
        rl.question("Width : ", (width) => {
          let rectangleOne = new Rectangle(length, width);
          rectangleOne.introduce("Geometry");
          rectangleOne.calculateArea();
          rectangleOne.calculateCircumference();
          rl.close();
        });
      });
      break;
    case '3':
      rl.question("Base : ", (base) => {
        rl.question("Height : ", (height) => {
          let triangleOne = new Triangle(base, height);
          triangleOne.introduce("Geometry");
          triangleOne.calculateArea();
          triangleOne.calculateCircumference();
          rl.close();
        });
      });
      break;
    case '4':
      rl.question("Length : ", (length) => {
        rl.question("Width : ", (width) => {
          rl.question("Height : ", (height) => {
            let beamOne = new Beam(length, width, height);
            beamOne.introduce("Geometry");
            beamOne.calculateVolume();
            rl.close();
          });
        });
      });
      break;
    case '5':
      rl.question("Length : ", (length) => {
        let cubeOne = new Cube(length);
        cubeOne.introduce("Geometry");
        cubeOne.calculateVolume();
        rl.close();
      });
      break;
    case '6':
      rl.question("Radius : ", (radius) => {
        rl.question("Height : ", (height) => {
          let tubeOne = new Tube(radius, height);
          tubeOne.introduce("Geometry");
          tubeOne.calculateVolume();
          rl.close();
        });
      });
      break;
    case '7':
      rl.question("Radius : ", (radius) => {
        rl.question("Height : ", (height) => {
          let tubeOne = new Cone(radius, height);
          tubeOne.introduce("Geometry");
          tubeOne.calculateVolume();
          rl.close();
        });
      });
      break;
    default:
      console.log("Tidak ada pilihan !");
      rl.close();
  }
});
