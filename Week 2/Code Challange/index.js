const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
  // Code Here
  let filtered = clean(data);
  for (let i = 0; i < filtered.length; i++) {
    for (let j = 0; j < filtered.length - i - 1; j++) {
      if (filtered[j] > filtered[j + 1]) {
        let temp = filtered[j];
        filtered[j] = filtered[j + 1];
        filtered[j + 1] = temp;
      }
    }
  } 
  return filtered;
}

// Should return array
function sortDecending(data) {
  // Code Here
  let filtered = clean(data);
  for (let i = 0; i < filtered.length; i++) {
    for (let j = 0; j < filtered.length - i - 1; j++) {
      if (filtered[j] < filtered[j + 1]) {
        let temp = filtered[j];
        filtered[j] = filtered[j + 1];
        filtered[j + 1] = temp;
      }
    }
  } 
  return filtered;
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
