const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

/*function CalculateBeam(p,l,t) {
    let VolumeBeam = p*l*t;
    console.log("Panjang : "+p);
    console.log("Lebar : "+l);
    console.log("Tinggi : "+t);
    console.log("Volume Beam : "+VolumeBeam+" cm3");
    return VolumeBeam;
}

function CalculateCone(r,t) {
    let VolumeCone = 1/3*Math.PI*(r**2);
    console.log("Jari-jari : "+r);
    console.log("Tinggi : "+t);
    console.log("Volume Cone : "+VolumeCone+" cm3");
    return VolumeCone;
}

CalculateBeam(7,5,10);
CalculateBeam(7,5,10);
CalculateBeam(7,5,10);
CalculateCone(7,10);*/

function beam(length, width, height) {
    return length*width*height;
}

function input() {
    rl.question("Length: ", length => {
      rl.question("Width: ", (width) => {
        rl.question("Height: ", (height) => {
          if (!isNaN(length) && !isNaN(width) && !isNaN(height)) {
            console.log(`\nBeam: ${beam(length, width, height)}`);
            rl.close();
          } else {
            console.log(`Length, Width and Height must be a number\n`);
            input();
          }
        });
      });
    });
  }
  input();
